from __future__ import print_function, division, absolute_import
import numpy as np
from numba import jit
from read_and_save import load_sparse_csr, get_test_sets, get_train_sets, load_svd_factor
from time import time
import os
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OMP_NUM_THREADS'] = '1'
from mpi4py import MPI
comm = MPI.COMM_WORLD
MPIrank = comm.Get_rank()
MPIsize = comm.Get_size()
#import ipdb
from scipy import sparse as sp
#MPIrank = 0
#MPIsize = 1

def mynpzloader(filename):
    data = np.load(filename)
    return dict(data)

def read_and_bcast(filename, read_func):
    if MPIrank == 0:
        matrix = read_func(filename)
    else:
        matrix = None
    matrix = comm.bcast(matrix, root=0)
    return matrix

def check_criterias(test, piv, coef, K, threshold):
    max_items = 500000000
    block_count = (test.shape[0]*test.shape[1]-1)//max_items+1
    block_size = (test.shape[0]-1)//block_count+1
    maxK = max(K)
    precision = np.zeros((test.shape[0], len(K)), dtype=np.float)
    recall = np.zeros((test.shape[0], len(K)), dtype=np.float)
    for block in range(block_count):
        start_row = block*block_size
        end_row = min((block+1)*block_size, test.shape[0])
        cur_test = test[start_row:end_row]
        approx = test[start_row:end_row, piv].dot(coef.T)
        approx[:, piv] = approx.min()-1
        partition = approx.argpartition(approx.shape[1]-maxK, axis=1)[:,-maxK:]
        ind = np.arange(end_row-start_row).reshape(-1, 1) * np.ones((1, maxK), dtype=np.int)
        top_recom = partition[ind, approx[ind, partition].argsort(axis=1)]
        # Compute precision
        for j in range(len(K)):
            cur_K = K[j]
            hits_per_row = (cur_test[ind[:, :cur_K], top_recom[:, -cur_K:]] >= threshold).sum(axis=1).astype(np.float).getA().reshape(-1)
            #ipdb.set_trace()
            good_ratings_per_row = ((cur_test >= threshold).sum(axis=1) - (cur_test[:, piv] >= threshold).sum(axis=1)).astype(np.float).getA().reshape(-1)
            precision[start_row:end_row, j] += hits_per_row/cur_K
            sort_order = good_ratings_per_row.argsort()
            only_zeros = good_ratings_per_row.searchsorted(1, side='left', sorter = sort_order)
            indexes = sort_order[:only_zeros]
            good_ratings_per_row[indexes] = 1.
            recall[start_row:end_row, j] += hits_per_row/good_ratings_per_row
    return precision, recall

def check_most_reprtype(filename, matrix, n_parts, rank, K, threshold):
    user_test_set = get_test_sets(matrix, n_parts)
    train_data_set = get_train_sets(matrix, n_parts)
    max_rank = max(rank)
    for i in range(n_parts):
        U = read_and_bcast('{}-{}-svdfactor.npz'.format(filename, i), load_svd_factor)
        #ipdb.set_trace()
        total_rows = user_test_set[i][1]-user_test_set[i][0]
        rows = total_rows//MPIsize*np.arange(MPIsize+1)
        diff = total_rows-rows[-1]
        for j in range(diff):
            rows[-1-j] += diff-j
        rows += user_test_set[i][0]
        my_start_row = rows[MPIrank]
        my_end_row = rows[MPIrank+1]
        test_data = matrix[my_start_row:my_end_row]
        train_data = train_data_set[i]
        piv = (-(train_data > 0).sum(axis=0).getA().reshape(-1)).argsort()
        #ipdb.set_trace()
        matrixcoef_precision = np.zeros((len(rank), len(K)))
        matrixcoef_recall = np.zeros((len(rank), len(K)))
        for i_rank in range(len(rank)):
            r = rank[i_rank]
            tmp_piv = piv[:r]
            tmp_piv_data = train_data[:, tmp_piv]
            matrixcoef_C = np.linalg.solve((tmp_piv_data.T.dot(tmp_piv_data)).toarray(), (tmp_piv_data.T.dot(train_data)).toarray()).T
            matrixcoef_precision[i_rank], matrixcoef_recall[i_rank] = check_criterias(test_data, tmp_piv, matrixcoef_C, K, threshold)
        all_matrixcoef_precision = comm.gather(matrixcoef_precision, root=0)
        all_matrixcoef_recall = comm.gather(matrixcoef_recall, root=0)
        if MPIrank == 0:
            for j in range(1, MPIsize):
                #svdcoef_precision += all_svdcoef_precision[j]
                #svdcoef_recall += all_svdcoef_recall[j]
                matrixcoef_precision += all_matrixcoef_precision[j]
                matrixcoef_recall += all_matrixcoef_recall[j]
            #svdcoef_precision /= total_rows
            #svdcoef_recall /= total_rows
            matrixcoef_precision /= total_rows
            matrixcoef_recall /= total_rows
            #np.savez('{}-{}-rectmaxvol_precision.npz'.format(filename, i), rank = rank, stop = stop, K = K, svdcoef_precision = svdcoef_precision, svdcoef_recall = svdcoef_recall, matrixcoef_precision = matrixcoef_precision, matrixcoef_recall = matrixcoef_recall)
            np.savez('{}-{}-most_precision_wo.npz'.format(filename, i), rank = rank, K = K, matrixcoef_precision = matrixcoef_precision, matrixcoef_recall = matrixcoef_recall)

def check_base_reprtype(filename, matrix, n_parts, K, threshold):
    user_test_set = get_test_sets(matrix, n_parts)
    train_data_set = get_train_sets(matrix, n_parts)
    for i in range(n_parts):
        U = read_and_bcast('{}-{}-svdfactor.npz'.format(filename, i), load_svd_factor)
        loader = read_and_bcast('{}-{}-rectmaxvol.npz'.format(filename, i), mynpzloader)
        piv, rank, stop = loader['piv'], loader['rank'], loader['stop']
        #ipdb.set_trace()
        total_rows = user_test_set[i][1]-user_test_set[i][0]
        rows = total_rows//MPIsize*np.arange(MPIsize+1)
        diff = total_rows-rows[-1]
        for j in range(diff):
            rows[-1-j] += diff-j
        rows += user_test_set[i][0]
        my_start_row = rows[MPIrank]
        my_end_row = rows[MPIrank+1]
        test_data = matrix[my_start_row:my_end_row]
        train_data = train_data_set[i]
        #svdcoef_precision = np.zeros((len(rank), len(rank)+1, len(K)))
        #svdcoef_recall = np.zeros((len(rank), len(rank)+1, len(K)))
        matrixcoef_precision = np.zeros((my_end_row-my_start_row, len(rank), len(rank)+1, len(K)))
        matrixcoef_recall = np.zeros((my_end_row-my_start_row, len(rank), len(rank)+1, len(K)))
        for i_rank in range(len(rank)):
            r = rank[i_rank]
            #tmp_U = U[:,:r].T
            for i_size in range(i_rank, len(rank)):
                size = rank[i_size]
                tmp_piv = piv[i_rank][:size]
                #svdcoef_C = np.linalg.lstsq(tmp_U[:, tmp_piv], tmp_U)[0].T
                tmp_piv_data = train_data[:, tmp_piv]
                matrixcoef_C = np.linalg.solve((tmp_piv_data.T.dot(tmp_piv_data)).toarray(), (tmp_piv_data.T.dot(train_data)).toarray()).T
                #svdcoef_precision[i_rank, i_size], svdcoef_recall[i_rank, i_size] = check_criterias(test_data, tmp_piv, svdcoef_C, K, threshold)
                matrixcoef_precision[:, i_rank, i_size, :], matrixcoef_recall[:, i_rank, i_size, :] = check_criterias(test_data, tmp_piv, matrixcoef_C, K, threshold)
            tmp_piv = piv[i_rank][:stop[i_rank]]
            #svdcoef_C = np.linalg.lstsq(tmp_U[:, tmp_piv], tmp_U)[0].T
            tmp_piv_data = train_data[:, tmp_piv]
            matrixcoef_C = np.linalg.solve((tmp_piv_data.T.dot(tmp_piv_data)).toarray(), (tmp_piv_data.T.dot(train_data)).toarray()).T
            #svdcoef_precision[i_rank, -1], svdcoef_recall[i_rank, -1] = check_criterias(test_data, tmp_piv, svdcoef_C, K, threshold)
            matrixcoef_precision[:, i_rank, -1, :], matrixcoef_recall[:, i_rank, -1, :] = check_criterias(test_data, tmp_piv, matrixcoef_C, K, threshold)
        #all_svdcoef_precision = comm.gather(svdcoef_precision, root=0)
        #all_svdcoef_recall = comm.gather(svdcoef_recall, root=0)
        #if MPIrank == 0:
        #    ipdb.set_trace()
        all_matrixcoef_precision = comm.gather(matrixcoef_precision, root=0)
        all_matrixcoef_recall = comm.gather(matrixcoef_recall, root=0)
        if MPIrank == 0:
            """
            for j in range(1, MPIsize):
                #svdcoef_precision += all_svdcoef_precision[j]
                #svdcoef_recall += all_svdcoef_recall[j]
                matrixcoef_precision += all_matrixcoef_precision[j]
                matrixcoef_recall += all_matrixcoef_recall[j]
            #svdcoef_precision /= total_rows
            #svdcoef_recall /= total_rows
            matrixcoef_precision /= total_rows
            matrixcoef_recall /= total_rows
            #np.savez('{}-{}-rectmaxvol_precision.npz'.format(filename, i), rank = rank, stop = stop, K = K, svdcoef_precision = svdcoef_precision, svdcoef_recall = svdcoef_recall, matrixcoef_precision = matrixcoef_precision, matrixcoef_recall = matrixcoef_recall)
            np.savez('{}-{}-rectmaxvol_precision_wo.npz'.format(filename, i), rank = rank, stop = stop, K = K, matrixcoef_precision = matrixcoef_precision, matrixcoef_recall = matrixcoef_recall)
            """
            np.savez('{}-{}-rectmaxvol_precision_wo_all.npz'.format(filename, i), rank = rank, stop = stop, K = K, matrixcoef_precision = np.vstack(all_matrixcoef_precision), matrixcoef_recall = np.vstack(all_matrixcoef_recall))

if __name__ == "__main__":
    t0 = time()
    if MPIrank == 0:
        print('MPIsize: ', MPIsize)
    rank = np.arange(5, 105, 5)
    
    matrix = read_and_bcast('movielens-20m.npz', load_sparse_csr)
    check_base_reprtype('movielens-20m-repritems', matrix, 5, [1,3,5,10], 4)
