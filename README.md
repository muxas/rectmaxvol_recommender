About
=====
This repository is for those, who want to apply rectangular maximum volume submatrices in the context of recommender systems. This repository is cited in corresponding proceedings of IEEE ICDM 2016, Barcelona.

Requirements
============
Scripts from this repository rely on maxvolpy python package: https://bitbucket.org/muxas/maxvolpy . Please install it before running anything from this repository.

FILES
=====

read\_and\_save.py: routines to read movielens data, save it as a sparse CSR
    matrix and load it any time.

test.py: script to read movielens data, save it in 'npz' file, divide it
    into training and test sets, compute PureSVD for each training set and
    get representative items by maxvol and rect\_maxvol for
    movielens 20million dataset.

parallel\_criterias.py: routines and script to use MPI to find Precision@K
    and Recall@K for K=[1, 3, 5, 10] for movielens 20million dataset

coverage.py: routines and script to compute coverage and diversity of
    results for movielens 20million dataset.

How to run
==========

At first run 'python test.py' to compute SVD of training sets and
to get representative items by maxvol and rect\_maxvol. Then, run
'python coverage.py' to compute coverage and diversity and then run
'python parallel\_criteris.py' or 'mpiexec -n 10 python
parallel\_criterias.py' to compute precision@K and recall@K.