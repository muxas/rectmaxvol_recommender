from __future__ import print_function
import numpy as np
from maxvolpy.maxvol import rect_maxvol, maxvol
from scipy.sparse import lil_matrix, csr_matrix, vstack
from time import time
from pypropack import svdp
from glob import glob

def read_dat(fname):
    with open(fname, 'r') as fd:
        lines = fd.readlines()
        print('Data is successfully read to RAM')
    size = len(lines)
    user = np.ndarray(size, dtype=np.int)
    movie = np.ndarray(size, dtype=np.int)
    rating = np.ndarray(size, dtype=np.float)
    for i in range(size):
        tmp = lines[i].strip().split('::')
        user[i] = int(tmp[0])
        movie[i] = int(tmp[1])
        rating[i] = float(tmp[2])
    del lines
    print('Data is parsed into 3 arrays')
    user_id = np.array(list(set(user)), dtype=np.int)
    np.random.shuffle(user_id)
    user_order = user_id.argsort()
    movie_id = np.array(list(set(movie)), dtype=np.int)
    np.random.shuffle(movie_id)
    movie_order = movie_id.argsort()
    matrix = lil_matrix((user_id.size, movie_id.size), dtype=np.float)
    real_row = user_order[user_id.searchsorted(user, sorter=user_order)]
    real_column = movie_order[movie_id.searchsorted(movie, sorter=movie_order)]
    for i in range(size):
        matrix[real_row[i],real_column[i]] = rating[i]
    print('Data is in lil_matrix, rows and columns are shuffled, no zero rows'
            ' or columns')
    return matrix.tocsr()

def read_csv(fname):
    with open(fname, 'r') as fd:
        lines = fd.readlines()
        print('Data is successfully read to RAM')
    size = len(lines)-1
    user = np.ndarray(size, dtype=np.int)
    movie = np.ndarray(size, dtype=np.int)
    rating = np.ndarray(size, dtype=np.float)
    for i in range(size):
        tmp = lines[i+1].strip().split(',')
        user[i] = int(tmp[0])
        movie[i] = int(tmp[1])
        rating[i] = float(tmp[2])
    del lines
    print('Data is parsed into 3 arrays')
    user_id = np.array(list(set(user)), dtype=np.int)
    np.random.shuffle(user_id)
    user_order = user_id.argsort()
    movie_id = np.array(list(set(movie)), dtype=np.int)
    np.random.shuffle(movie_id)
    movie_order = movie_id.argsort()
    matrix = lil_matrix((user_id.size, movie_id.size), dtype=np.float)
    real_row = user_order[user_id.searchsorted(user, sorter=user_order)]
    real_column = movie_order[movie_id.searchsorted(movie, sorter=movie_order)]
    for i in range(size):
        matrix[real_row[i],real_column[i]] = rating[i]
    print('Data is in lil_matrix, rows and columns are shuffled,'
            ' no zero rows or columns')
    return matrix.tocsr()

def save_sparse_csr(filename,array):
    np.savez(filename,data = array.data ,indices=array.indices,
             indptr =array.indptr, shape=array.shape )

def load_sparse_csr(filename):
    loader = np.load(filename)
    return csr_matrix((  loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])

def get_test_sets(matrix, n_parts):
    step_rows = (matrix.shape[0]-1)/n_parts+1
    res = []
    for i in range(n_parts):
        res.append([i*step_rows, min((i+1)*step_rows, matrix.shape[0])])
    return res

def get_train_sets(matrix, n_parts):
    step_rows = (matrix.shape[0]-1)/n_parts+1
    res = []
    for i in range(n_parts):
        start = i*step_rows
        end = min((i+1)*step_rows, matrix.shape[0])
        #print(start,end)
        train = vstack([matrix[:start], matrix[end:]])
        res.append(train)
    return res

def divide_and_svd(matrix, n_parts, base_fname):
    train_set = get_train_sets(matrix, n_parts)
    for i in range(n_parts):
       U = svdp(train_set[i], 100)[2].T
       #print(U.shape)
       np.savez(base_fname.format(i), shape=U.shape, data=U)

def load_svd_factor(fname):
    loader = np.load(fname)
    return loader['data'].reshape(loader['shape'])

def maxvol_benchmark(base_fname, n_parts, rank):
        for i in range(n_parts):
            U = load_svd_factor('{}-{}-svdfactor.npz'.format(base_fname, i))
            piv = []
            for r in rank:
                piv.append(maxvol(U[:,:r])[0])
            np.savez('{}-{}-maxvol.npz'.format(base_fname, i), piv=piv,
                    rank=rank)

def rect_maxvol_benchmark(base_fname, n_parts, rank):
    for i in range(n_parts):
        U = load_svd_factor('{}-{}-svdfactor.npz'.format(base_fname, i))
        piv = []
        stop = []
        for r in rank:
            piv.append(rect_maxvol(U[:,:r], maxK=100, minK=100)[0])
            stop.append(rect_maxvol(U[:,:r])[0].size)
        np.savez('{}-{}-rectmaxvol.npz'.format(base_fname, i), piv=piv,
                stop=stop, rank=rank)
