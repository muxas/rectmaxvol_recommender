from read_and_save import read_csv, save_sparse_csr, load_sparse_csr,\
        divide_and_svd, rect_maxvol_benchmark
import os, sys
import numpy as np

def run_all(matrix):
    pass


ParamError = ValueError('Please provide one of following parameters: ml-20m')
MovieLens20m_ratings = '../www_recommender/ml-20m/ratings.csv'
n_parts = 5 # Divide dataset into training and test sets into n_parts parts.
#               Every time 1 of this parts is used as test set, all others
#               are used as training sets.
#               n_parts = 5 means there will be total 5 different runs.
#               Each run with 1 test set and 4 training sets.
#               Each time different part of dataset plays role of test set.
represent = 'repritems' # find representative items
# respresent = 'reprusers' # find representative users
check_rank = np.arange(5, 105, 5) # Set test ranks as 5, 10, 15, ..., 95, 100
if __name__ == '__main__':
    print 'Working on Movielens data with 20 million ratings'
    # Load dataset as a sparse matrix (from dataset itself or
    # from sparse matrix)
    fname = 'movielens-20m.npz'
    if not os.path.exists(fname) or not os.path.isfile(fname):
        dataset = MovieLens20m_ratings
        matrix = read_csv(dataset)
        save_sparse_csr(fname, matrix)
        print 'Sparse matrix is written into '+fname
    else:
        matrix = load_sparse_csr(fname)
        print 'Loaded sparse matrix from '+fname
    # Divide data set into training sets, compute PureSVD of
    # each and save it
    svd_done = True
    base_fname = 'movielens-20m-'+represent+'-{}-svdfactor.npz'
    for i in range(n_parts):
        fname = base_fname.format(i)
        if not os.path.exists(fname) or not os.path.isfile(fname):
            svd_done = False
            break
    if not svd_done:
        print 'Computing PureSVD of each training set'
        divide_and_svd(matrix, n_parts, base_fname)
        print 'Done with computing PureSVD of all training sets'
    else:
        print 'Skipping PureSVD, since it was done previously'
    # Perform maxvol and rect_maxvol to find reprentatives and
    # save results
    maxvol_done = True
    base_fname = 'movielens-20m-'+represent+'-{}-rectmaxvol.npz'
    for i in range(n_parts):
        fname = base_fname.format(i)
        if not os.path.exists(fname) or not os.path.isfile(fname):
            maxvol_done = False
            break
    base_fname = 'movielens-20m-'+represent
    if not maxvol_done:
        print 'Computing maxvol and rect_maxvol for each training set'
        rect_maxvol_benchmark(base_fname, n_parts, check_rank)
        print 'Done with maxvol and rect_maxvol'
    else:
        print 'Skipping maxvol and rect_maxvol since it was done previously'
