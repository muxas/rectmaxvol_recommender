from __future__ import print_function, absolute_import, division

import numpy as np
from read_and_save import load_sparse_csr, get_test_sets, get_train_sets, load_svd_factor

def rectmaxvol_coverage(filename, matrix):
    loader = np.load(filename)
    piv, rank, stop = loader['piv'], loader['rank'], loader['stop']
    ans = np.zeros((len(rank), len(rank)+1), dtype=np.float)
    for i in range(len(rank)):
        for j in range(i, len(rank)):
            tmp_piv = piv[i][:rank[j]]
            ans[i, j] = ((matrix[:, tmp_piv] > 0.).sum(axis=1) > 0).sum()/matrix.shape[0]
        tmp_piv = piv[i][:stop[i]]
        ans[i, -1] = ((matrix[:, tmp_piv] > 0.).sum(axis=1) > 0).sum()/matrix.shape[0]
        #print(i)
    return ans

def most_coverage(train_data, rank, matrix):
    ans = np.zeros(len(rank), dtype=np.float)
    piv = (-(train_data > 0).sum(axis=0).getA().reshape(-1)).argsort()
    for i in range(len(rank)):
        tmp_piv = piv[:rank[i]]
        ans[i] = ((matrix[:, tmp_piv] > 0.).sum(axis=1) > 0).sum()/matrix.shape[0]
    return ans

def reprtype_coverage(filename, matrix, n_parts):
    train_data = get_train_sets(matrix, n_parts)
    rank = np.arange(5, 105, 5, dtype=np.int)
    for i in range(n_parts):
        ans = rectmaxvol_coverage('{}-{}-rectmaxvol.npz'.format(filename, i), matrix)
        np.savez('{}-{}-rectmaxvol_coverage.npz'.format(filename, i), coverage=ans)
        ans = most_coverage(train_data[i], rank, matrix)
        np.savez('{}-{}-most_coverage.npz'.format(filename, i), coverage=ans)

def rectmaxvol_diversity_0(filename, matrix):
    loader = np.load(filename)
    piv, rank, stop = loader['piv'], loader['rank'], loader['stop']
    ans = np.zeros((len(rank), len(rank)+1), dtype=np.float)
    for i in range(len(rank)):
        for j in range(i, len(rank)):
            size = rank[j]
            tmp_piv = piv[i][:size]
            ans[i, j] = ((matrix[:, tmp_piv].toarray() <= 0.1*size).sum(axis=1) > 0).sum()/matrix.shape[0]
        tmp_piv = piv[i][:stop[i]]
        ans[i, -1] = ((matrix[:, tmp_piv].toarray() <= 0.1*size).sum(axis=1) > 0).sum()/matrix.shape[0]
        #print(i)
    return ans

def most_diversity_0(train_data, rank, matrix):
    ans = np.zeros(len(rank), dtype=np.float)
    piv = (-(train_data > 0).sum(axis=0).getA().reshape(-1)).argsort()
    for i in range(len(rank)):
        size = rank[i]
        tmp_piv = piv[:size]
        tmp_mat = matrix[:, tmp_piv].toarray()
        ans[i] = ((tmp_mat <= 0.1*size).sum(axis=1) > 0).sum()/matrix.shape[0]
    return ans

def rectmaxvol_diversity_1(filename, matrix):
    loader = np.load(filename)
    piv, rank, stop = loader['piv'], loader['rank'], loader['stop']
    ans = np.zeros((len(rank), len(rank)+1), dtype=np.float)
    for i in range(len(rank)):
        for j in range(i, len(rank)):
            size = rank[j]
            tmp_piv = piv[i][:size]
            tmp_mat = matrix[:, tmp_piv].toarray()
            ans[i, j] = (((tmp_mat <= 0.1*size)*(tmp_mat > 0)).sum(axis=1) > 0).sum()/matrix.shape[0]
        tmp_piv = piv[i][:stop[i]]
        tmp_mat = matrix[:, tmp_piv].toarray()
        ans[i, -1] = (((tmp_mat <= 0.1*size)*(tmp_mat > 0)).sum(axis=1) > 0).sum()/matrix.shape[0]
        #print(i)
    return ans

def most_diversity_1(train_data, rank, matrix):
    ans = np.zeros(len(rank), dtype=np.float)
    piv = (-(train_data > 0).sum(axis=0).getA().reshape(-1)).argsort()
    for i in range(len(rank)):
        size = rank[i]
        tmp_piv = piv[:size]
        tmp_mat = matrix[:, tmp_piv].toarray()
        ans[i] = (((tmp_mat <= 0.1*size)*(tmp_mat > 0)).sum(axis=1) > 0).sum()/matrix.shape[0]
    return ans

def reprtype_diversity(filename, matrix, n_parts):
    train_data = get_train_sets(matrix, n_parts)
    rank = np.arange(5, 105, 5, dtype=np.int)
    for i in range(n_parts):
        ans0 = rectmaxvol_diversity_0('{}-{}-rectmaxvol.npz'.format(filename, i), matrix)
        ans1 = rectmaxvol_diversity_1('{}-{}-rectmaxvol.npz'.format(filename, i), matrix)
        np.savez('{}-{}-rectmaxvol_diversity.npz'.format(filename, i), diversity_with0=ans0, diversity_wo0=ans1)
        ans0 = most_diversity_0(train_data[i], rank, matrix)
        ans1 = most_diversity_1(train_data[i], rank, matrix)
        np.savez('{}-{}-most_diversity.npz'.format(filename, i), diversity_with0=ans0, diversity_wo0=ans1)

if __name__ == '__main__':
    matrix = load_sparse_csr('movielens-20m.npz')
    reprtype_coverage('movielens-20m-repritems', matrix, 5)
    reprtype_diversity('movielens-20m-repritems', matrix, 5))
